<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [

                'name' => '3 Days',
                'percent' => '1.50',
                'color' => '3',
                'icon' => 'fa-laptop',
                'children' => [

                    ['name' => '50% in 3 Days'],


                     ]
            ],
            
          


        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}
