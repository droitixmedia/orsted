<?php

namespace openjobs\Http\Controllers\Resume;

use Mail;
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use openjobs\{Area, Resume};
use openjobs\Http\Requests\StoreResumeContactFormRequest;
use openjobs\Mail\ResumeContactCreated;

class ResumeContactController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth']);
    }

    public function store(StoreResumeContactFormRequest $request, Area $area, Resume $resume)
    {
        Mail::to($resume->user)
        ->cc($resume->companyemail)
        ->queue(
            new ResumeContactCreated($resume, $request->user(), $request->message)
        );

        return back()->withSuccess("We have sent your message");
    }
}
