<?php

namespace openjobs\Http\Controllers\Category;

use openjobs\{Area, Category};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Area $area)
    {

        $categories = Category::withListingsInArea($area)->get()->toTree();

        return view('categories.v2index', compact('categories'));
}

}