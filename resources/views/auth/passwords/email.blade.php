@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   
<div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="{{url('/')}}" class="logo-link">
                                        <img class="logo-light logo-img logo-img-lg" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                        <img class="logo-dark logo-img logo-img-lg" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                    </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Reset password</h5>
                                        <div class="nk-block-des">
                                            <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
                                             @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                              <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                            <a class="link link-primary link-sm" href="#">Need Help?</a>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} required form-control-lg" name="email" placeholder="Your email address" value="{{ old('email') }}" >
                                        </div>
                                          @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-lg btn-primary btn-block">Send Reset Link</button>
                                    </div>
                                </form><!-- form -->
                                <div class="form-note-s2 pt-5">
                                    <a href="{{url('login')}}"><strong>Return to login</strong></a>
                                </div>
                            </div><!-- .nk-block -->
        

@endsection
