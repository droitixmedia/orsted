@extends('layouts.userapp')
@section('title')
Viewed Jobs  | Openjobs360
@endsection
@section('content')
@if(Auth::check())
        @if (Auth::user()->truecompany())
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{$user->companyname}}</h5>
                                <p>{{$user->province}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                        <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Company Profile</a></li>
                            <li><a href="{{ route('listings.create', [$area]) }}"><span class="flaticon-resume"></span> Post a New Job</a></li>
                            <li ><a href="{{ route('listings.published.index', [$area]) }}"><span class="flaticon-paper-plane"></span> Manage Live Jobs</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-analysis"></span> Shortlisted Resumes</a></li>
                            <li><a class="active" href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>


                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('listings.store', [$area]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Showing your last {{ $indexLimit }} viewed jobs.</h4>
                            </div>
                             <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon"><span class="flaticon-work"></span></div>
                                    <div class="details"><h4>Post jobs</h4></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon style2"><span class="flaticon-resume"></span></div>
                                    <div class="details"><h4>Get Applications</h4></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon style3"><span class="flaticon-work"></span></div>
                                    <div class="details"><h4>Recruit</h4></div>
                                </div>
                            </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>


                          <div class="col-lg-12">
                            <div class="cnddte_fvrt_job candidate_job_reivew">
                                <div class="table-responsive job_review_table">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Job Title</th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                                <th scope="col">Date Advertised</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>


    @if ($listings->count())
        @each ('listings.partials._listing_viewed', $listings, 'listing')
    @else
        <p>You have not viewed any jobs yet!</p>
    @endif

                                        </tbody>
                                    </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@else

<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{$user->fullname}}</h5>
                                <p>{{$user->province}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                        <ul>
                            <li><a href="page-candidates-dashboard.html"><span class="flaticon-dashboard"></span> Dashboard</a></li>
                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li><a href="page-candidates-my-resume.html"><span class="flaticon-resume"></span> Resume</a></li>
                            <li><a href="page-candidates-applied-jobs.html"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="page-candidates-cv-manager.html"><span class="flaticon-analysis"></span> CV Manager</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Favourite Jobs</a></li>
                            <li class="active"><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>
                            <li><a href="page-candidates-review.html"><span class="flaticon-rating"></span> Reviews</a></li>
                            <li><a href="page-candidates-job-alert.html"><span class="flaticon-alarm"></span> Job Alerts</a></li>
                            <li><a href="page-candidates-change-password.html">


                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('listings.store', [$area]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Showing your last {{ $indexLimit }} viewed jobs.</h4>
                            </div>
                             <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon"><span class="flaticon-work"></span></div>
                                    <div class="details"><h4>Apply jobs</h4></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon style2"><span class="flaticon-resume"></span></div>
                                    <div class="details"><h4>Get Shortlisted</h4></div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="icon_boxs">
                                    <div class="icon style3"><span class="flaticon-work"></span></div>
                                    <div class="details"><h4>Get Hired</h4></div>
                                </div>
                            </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>


                          <div class="col-lg-12">
                            <div class="cnddte_fvrt_job candidate_job_reivew">
                                <div class="table-responsive job_review_table">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Job Title</th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                                <th scope="col">Date Advertised</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>


    @if ($listings->count())
        @each ('listings.partials._listing_viewed', $listings, 'listing')
    @else
        <p>You have not viewed any jobs yet!</p>
    @endif

                                        </tbody>
                                    </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endif
@endif

@endsection
