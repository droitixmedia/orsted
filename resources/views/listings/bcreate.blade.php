@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        <li class="nav-item">
                                            
                                        </li>
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h2 class="title">Withdraw Bonus</h2>
                                    @if($errors->any())
<h4 style="color:red">{{$errors->first()}}</h4>
@endif
                                            @if(session('success'))
    <h3 style="color:green">{{session('success')}}</h3>

@endif
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                    <form action="{{ route('listings.store', [$area]) }}" method="post" class="buysell-form">@php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach

                                        <div class="buysell-field form-group">
                                            <div class="form-label-group">
                                                <label class="form-label" for="buysell-amount">Your Bonus Balance is {{ Auth::user()->area->unit }}{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}.00  </label>
                                            </div>
                                            <input type="hidden" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}">
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                        
                                          <input type="hidden" class="form-control" name="value" id="value" value="1">
                                           <input type="hidden" class="form-control" name="current" id="value" value="1">
                                         <input type="hidden" class="form-control" name="recommit" id="period" value="1">
                                         <input type="hidden" class="form-control" name="period" id="period" value="1">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                            
                                        <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#buy-coin">Withdraw Cash</button>
                                        </div><!-- .buysell-field -->
                                        <div class="form-note text-base text-center">Withdraw Bonus above the minimum withdrawal<a href="#"></a>.</div>
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form -->
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
            </div>
                <!-- content @e -->
@endsection
