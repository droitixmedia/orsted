@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('listings.create', [$area]) }}">Deposit </a>
                                        </li>
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h2 class="title">Deposit into your Orsted account</h2>
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                    <form action="{{ route('listings.store', [$area]) }}" method="post" class="buysell-form">

                                        <div class="buysell-field form-group">
                                            <div class="form-label-group">
                                                <label class="form-label" for="buysell-amount">Enter amount </label>
                                            </div>
                                            <div class="form-control-group">
                                                <div class="form-dropdown">
                                                   
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-indicator-caret" data-toggle="dropdown" data-offset="0,2">{{ Auth::user()->area->unit }}</a>
                                                       
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control form-control-lg form-control-number" id="buysell-amount" name="amount" placeholder="Enter Amount to Invest">
                                                
                                            </div>
                                            <div class="form-note-group">
                                                <span class="buysell-min form-note-alt">Minimum: {{ Auth::user()->area->unit }}300  or  $20</span>
                                               
                                            </div>
                                        </div><!-- .buysell-field -->
                                        
                                        <input type="hidden" class="form-control" name="value" id="value" value="0.045">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                        <input type="hidden" class="form-control" name="current" id="current" value="0">
                                          <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                        <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#buy-coin">Create Deposit Order</button>
                                        </div><!-- .buysell-field -->
                                        <div class="form-note text-base text-center">Note: make sure your amount is not less than minimum<a href="#"></a>.</div>
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form -->
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
                <!-- content @e -->


@endsection
