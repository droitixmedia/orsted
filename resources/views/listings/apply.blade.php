@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('listings.create', [$area]) }}"></a>
                                        </li>
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h2 class="title">Withdrawals are sent to your ACCOUNT NUMBER On your profile.</h2>
                                    @if($errors->any())
<h4 style="color:red">{{$errors->first()}}</h4>
@endif
                                            @if(session('success'))
    <h3 style="color:green">{{session('success')}}</h3>

@endif
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                    <form action="{{ route('comments.store', [$listing->id]) }}" method="post" class="buysell-form">
                                                                          @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                  @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                   @php($multiplier = ($listing->amount-$listing->current))

                                        <div class="buysell-field form-group">
                                            <div class="form-label-group">
                                                <label class="form-label" for="buysell-amount">{{ Auth::user()->area->unit }} {{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount)-($listing->scrap)}} Available for withdrawal </label>
                                            </div>
                                            <div class="form-control-group">
                                                <input type="text" class="form-control form-control-lg form-control-number" id="buysell-amount" name="split" placeholder="Enter Amount">
                                                <div class="form-dropdown">
                                                   
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-indicator-caret" data-toggle="dropdown" data-offset="0,2">{{ Auth::user()->area->unit }}</a>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-note-group">
                                                <span class="buysell-min form-note-alt"></span>
                                               
                                            </div>
                                        </div><!-- .buysell-field -->
                                           <input type="hidden" class="form-control" name="body" id="body" value="1">
                                            <input type="hidden" class="form-control" name="category_id" id="body" value="2">
                            
                                        <div class="buysell-field form-action">
                                            <button type = "submit" class="btn btn-lg btn-block btn-primary"  >Withdraw Cash</button>
                                        </div><!-- .buysell-field -->
                                        <div class="form-note text-base text-center"><a href="#"></a>.</div>
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form -->
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
                <!-- content @e -->
@endsection
