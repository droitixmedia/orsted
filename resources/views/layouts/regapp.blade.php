<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.reghead')

</head>
   <body class="nk-body bg-white npc-default has-aside no-touch nk-nio-theme dark-mode as-mobile" theme="dark">

  

          <div id="app">

  <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                   <!-- content @s -->
                <div class="nk-content ">




       @yield('content')


        
       

       

@include('layouts.partials.footer')


                  </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->



    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.6.0"></script>
    <script src="./assets/js/scripts.js?ver=2.6.0"></script>
</body>


</html>
