<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.head')

</head>
<body class="nk-body bg-white npc-default has-aside no-touch nk-nio-theme dark-mode as-mobile" theme="dark">>
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
       
 
  <div id="app">
   
@include('layouts.partials.sidebar')

  <!-- wrap @s -->
  <div class="nk-wrap ">
   @include('layouts.partials.navigation')




       @yield('content')


      

     @include('layouts.partials.footer')

        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
     <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.6.0"></script>
    <script src="./assets/js/scripts.js?ver=2.6.0"></script>
    <script src="./assets/js/charts/chart-crypto.js?ver=2.6.0"></script>
</body>
</html>
