<!-- main header @s -->
                <div class="nk-header nk-header-fluid nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="{{url('dashboard')}}" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo.png" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo.png" srcset="./images/logo.png" alt="logo-dark">
                                    <span class="nio-version"></span>
                                </a>
                            </div>
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="{{url('dashboard')}}">
                                        <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Get profits up to 7.5% daily with Ørsted <span></span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                   
                                                    <div class="user-name dropdown-indicator">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>{{Auth::user()->initials()}} </span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
                                                        <span class="sub-text">{{Auth::user()->email}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner user-account-info">

                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                         @php ($sum += (($multiplier)*($percentage*$days))-$listing->scrap)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach

                                                <h6 class="overline-title-alt">Profits Gained</h6>
                                                <div class="user-balance">{{ Auth::user()->area->unit }} {{$sum-$diff}}.00 <small class="currency currency-btc"></small></div>
                                                @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                                                <div class="user-balance-sub">Bonus Accumulated <span>
                                                    {{ Auth::user()->area->unit }} {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}.00<span class="currency currency-btc"></span></span></div>
                                                <a href="{{ route('listings.published.index', [$area]) }}" class="link"><span>Withdraw Funds</span> <em class="icon ni ni-wallet-out"></em></a>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="{{url('profile')}}"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="{{url('profile')}}"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                   
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span></a>
                                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                     </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dropdown notification-dropdown mr-n1">
                                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                                            <div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
                                        </a>




                                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-head">
                                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                                <a href="#">Mark All as Read</a>
                                            </div>

                                            <div class="dropdown-body">
                                            @foreach (auth()->user()->unreadNotifications as $notification)        @if(!empty($notification->data['notification_message']))
                                                <div class="nk-notification">
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">{{$notification->data['notification_message']}}<span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" data-notifications-id="{{$notification->id}}" onclick="markAsRead(event)">&times;</span>
                                </button></span></div>
                                                            <div class="nk-notification-time"></div>
                                                        </div>
                                                    </div><!-- .dropdown-inner -->
                                          
                                                </div>
                                                @endif
                                               @endforeach
                                            </div><!-- .nk-dropdown-body -->
                                            <div class="dropdown-foot center">
                                                <a href="#">View All</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- main header @e -->