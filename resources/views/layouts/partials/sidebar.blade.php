       <!-- sidebar @s -->
            <div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-sidebar-brand">
                        <a href="{{url('dashboard')}}" class="logo-link nk-sidebar-logo">
                            <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo.png" alt="logo">
                            <img class="logo-dark logo-img" src="./images/logo.png" srcset="./images/logo.png" alt="logo-dark">
                            <span class="nio-version"></span>
                        </a>
                    </div>
                    <div class="nk-menu-trigger mr-n2">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element">
                    <div class="nk-sidebar-body" data-simplebar>
                        <div class="nk-sidebar-content">
                            <div class="nk-sidebar-widget d-none d-xl-block">
                                <div class="user-account-info between-center">
                                    <div class="user-account-main">
                                        @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                             @php ($sum += (($multiplier)*($percentage*$days))-$listing->scrap)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                                        <h6 class="overline-title-alt">Profits Gained</h6>
                                        <div class="user-balance">{{ Auth::user()->area->unit }} {{$sum-$diff}}.00<small class="currency currency-btc"></small></div>
                                       
                                    </div>
                                    <a href="#" class="btn btn-white btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
                                </div>
                                <ul class="user-account-data gy-1">
                                    <li>
                                        <div class="user-account-label">
                                            <span class="sub-text">Bonus Accumulated</span>
                                        </div>
                                        @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                                        <div class="user-account-value">
                                            <span class="lead-text">{{ Auth::user()->area->unit }} {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}.00 <span class="currency currency-btc"></span></span>
                                            <span class="text-success ml-2">10% Bonus <em class="icon ni ni-arrow-long-up"></em></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="user-account-label">
                                            <span class="sub-text">Total Deposits</span>
                                        </div>
                                         @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                   
                            @if($listing->matched())


                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif


                     @endforeach
                                        <div class="user-account-value">
                                            <span class="sub-text">{{ Auth::user()->area->unit }} {{$sum}}.00<span class="currency currency-btc"></span></span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="user-account-actions">
                                    <ul class="g-3">
                                        <li><a href="{{ route('listings.create', [$area]) }}" class="btn btn-lg btn-primary"><span>Deposit</span></a></li>
                                        <li><a href="{{ route('listings.published.index', [$area]) }}" class="btn btn-lg btn-warning"><span>Withdraw</span></a></li>
                                    </ul>
                                </div>
                            </div><!-- .nk-sidebar-widget -->
                            <div class="nk-sidebar-widget nk-sidebar-widget-full d-xl-none pt-0">
                                <a class="nk-profile-toggle toggle-expand" data-target="sidebarProfile" href="#">
                                    <div class="user-card-wrap">
                                        <div class="user-card">
                                            <div class="user-avatar">
                                                <span>{{Auth::user()->initials()}}</span>
                                            </div>
                                            <div class="user-info">
                                                <span class="lead-text">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
                                                <span class="sub-text">{{Auth::user()->email}}</span>
                                            </div>
                                            <div class="user-action">
                                                <em class="icon ni ni-chevron-down"></em>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="nk-profile-content toggle-expand-content" data-content="sidebarProfile">
                                    <div class="user-account-info between-center">
                                        <div class="user-account-main">
                                                            @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                            @php ($sum += (($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                                            <h6 class="overline-title-alt">Account Balance</h6>
                                            <div class="user-balance">{{ Auth::user()->area->unit }} {{$sum-$diff}}.00 <small class="currency currency-btc"></small></div>
                                            
                                        </div>
                                        <a href="#" class="btn btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
                                    </div>
                                    <ul class="user-account-data">
                                       
                                    </ul>
                                   
                                    
                                </div>
                            </div><!-- .nk-sidebar-widget -->
                            <div class="nk-sidebar-menu">
                                <!-- Menu -->
                                <ul class="nk-menu">
                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">Menu</h6>
                                    </li>
                                    <li class="nk-menu-item">
                                        <a href="{{url('dashboard')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-dashboard"></em></span>
                                            <span class="nk-menu-text">Investment Panel</span>
                                        </a>
                                    </li>
                                   
                                    <li class="nk-menu-item">
                                        <a href="{{ route('listings.published.index', [$area]) }}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-wallet-alt"></em></span>
                                            <span class="nk-menu-text">Account Wallet</span>
                                        </a>
                                    </li>
                                    <li class="nk-menu-item">
                                        <a href="{{ route('listings.create', [$area]) }}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                                            <span class="nk-menu-text">Deposit</span>
                                        </a>
                                    </li>
                                    <li class="nk-menu-item">
                                        <a href="{{ route('listings.published.index', [$area]) }}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-repeat"></em></span>
                                            <span class="nk-menu-text">Withdraw Funds</span>
                                        </a>
                                    </li>
                                   
                                    <li class="nk-menu-item">
                                        <a href="{{url('profile')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-account-setting"></em></span>
                                            <span class="nk-menu-text">My Profile</span>
                                        </a>
                                    </li>
                                    <li class="nk-menu-item has-sub">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-files"></em></span>
                                            <span class="nk-menu-text">Bonus Pages</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                            <li class="nk-menu-item">
                                                <a href="{{url('referrals')}}" class="nk-menu-link"><span class="nk-menu-text">Your Downliners</span></a>
                                            </li>
                                            <li class="nk-menu-item">
                                                <a href="{{ route('listings.bcreate', [$area]) }}" class="nk-menu-link"><span class="nk-menu-text">Withdraw Bonus</span></a>
                                            </li>
                                           
                                        </ul><!-- .nk-menu-sub -->
                                    </li><!-- .nk-menu-item -->
                                    <li class="nk-menu-item">
                                        <a href="#" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-download-cloud"></em></span>
                                            <span class="nk-menu-text">Downloads</span>
                                        </a>
                                    </li>
                                    @if (session()->has('impersonate'))
                                     <li class="nk-menu-item">
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-stop"></em></span>
                                            <span class="nk-menu-text">Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @endif
                                    @role('admin')
                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">Admin Pages</h6>
                                    </li>
                                    <li class="nk-menu-item">
                                        <a href="{{ url('admin/impersonate')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Impersonate</span>
                                        </a>
                                    </li>
                                     <li class="nk-menu-item">
                                        <a href="{{ url('admin/listings')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Deposits</span>
                                        </a>
                                    </li>
                                        <li class="nk-menu-item">
                                        <a href="{{ url('admin/withdrawals')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Withdrawals</span>
                                        </a>
                                    </li>
                                     <li class="nk-menu-item">
                                        <a href="{{ url('admin/investments')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Investments</span>
                                        </a>
                                    </li>
                                      <li class="nk-menu-item">
                                        <a href="{{ url('admin/bonus')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Bonus Withdrawal</span>
                                        </a>
                                    </li>
                                        <li class="nk-menu-item">
                                        <a href="{{ url('admin/users')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Users</span>
                                        </a>
                                    </li>
                                    
                                    @endrole
                                </ul><!-- .nk-menu -->
                            </div><!-- .nk-sidebar-menu -->
                          
                            <div class="nk-sidebar-footer">
                                <ul class="nk-menu nk-menu-footer">
                                    <li class="nk-menu-item">
                                        <a href="#" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-help-alt"></em></span>
                                            <span class="nk-menu-text">Support</span>
                                        </a>
                                    </li>
                                    <li class="nk-menu-item ml-auto">
                                        <div class="dropup">
                                            <a href="#" class="nk-menu-link dropdown-indicator has-indicator" data-toggle="dropdown" data-offset="0,10">
                                                <span class="nk-menu-icon"><em class="icon ni ni-globe"></em></span>
                                                <span class="nk-menu-text">English</span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                                <ul class="language-list">
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/english.png" alt="" class="language-flag">
                                                            <span class="language-name">English</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/spanish.png" alt="" class="language-flag">
                                                            <span class="language-name">Español</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/french.png" alt="" class="language-flag">
                                                            <span class="language-name">Français</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/turkey.png" alt="" class="language-flag">
                                                            <span class="language-name">Türkçe</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul><!-- .nk-footer-menu -->
                            </div><!-- .nk-sidebar-footer -->
                        </div><!-- .nk-sidebar-content -->
                    </div><!-- .nk-sidebar-body -->
                </div><!-- .nk-sidebar-element -->
            </div>
            <!-- sidebar @e -->