@extends('layouts.regapp')


@section('title')
  Proffesional CV Writing Services in South Africa from R300 | Openjobs360
@endsection

@section('description')
Proffesional CV Writing Service That is Deeply Customized For The South African Job Market.Our Team of Recruiting Agents Will Tailor Your CV to Match Your Desired Industry.Starting From Just R300.
@endsection

@section('content')


<!-- Inner Page Breadcrumb -->
    <section class="inner_page_breadcrumb bgc-f0 pt30 pb30" aria-label="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="breadcrumb_title float-left">CV Writing</h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="{{ url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">CV Writing</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- About Text Content -->
    <section class="about-section bgc-fa">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about_content">
                        <h3>Don't let a horrible CV block your career</h3>
                        <p class="color-black22 mt30">Our CV writing team constitutes of recruiting agents, who have years of experience with different kinds of CV content and know exactly what is needed for what job sector!</p>
                        <p>With hundreds of CVs to plough through, an employer won't initially spend more than about 30 seconds looking at each individual CV. The secret of our CV service lies in knowing what to include, what not to include, and what kind of a spin to put on your CV, to ensure YOU stand out and not just the document - to give you the very best possible chance of getting the job you want.
                        <a class="btn btn-thm mt15" href="{{url('bookacv')}}">Book now!<span class="fa fa-long-arrow-right pl10"></span></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about_thumb mt50">
                        <img class="img-fluid" src="images/about/1.png" alt="1.png">
                    </div>
                </div>
            </div>
        </div>
    </section>

       <!-- How It's Work -->
    <section class="popular-job">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="ulockd-main-title">
                        <h3 class="mt0">How we design your CV</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4 prpl5">
                    <div class="icon_box_hiw">
                        <div class="icon"><div class="list_tag float-right"><p>1</p></div><span class="flaticon-resume"></span></div>
                        <div class="details">
                            <h4>Book and submit details</h4>
                            <p>Once you book a package, we will request your old cv or any information format to kickstart the cv writing process.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 prpl5 mt20-xxsd">
                    <div class="icon_box_hiw">
                        <div class="icon middle"><div class="list_tag float-right"><p>2</p></div><span class="flaticon-paper-plane"></span></div>
                        <div class="details">
                            <h4>Team works on your CV</h4>
                            <p>Our team of proffesionals will begin to work on your CV and make it industry specific.The process will take 2 working days.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 prpl5 mt20-xxsd">
                    <div class="icon_box_hiw">
                        <div class="icon"><div class="list_tag float-right"><p>3</p></div><span class="flaticon-doc"></span></div>
                        <div class="details">
                            <h4>Revise and Send your CV formats</h4>
                            <p>We will revise your CV together and allow you to make changes before we E-mail all the desired formats to you.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Our Pricing Table -->
    <section class="our-pricing bgc-fa">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="ulockd-main-title mb50">
                        <h3 class="mt0">Our CV writing Packages</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 p0 prpl5-sm">
                    <div class="pricing_table">
                        <h3 class="text-center">Entry Level</h3>

                        <div class="pt_header_four">
                            <div class="pt_tag_four"><span><h2 style="color:#49D49D;">R300</h2></span></div>
                            <h4>For Matric and General Jobs</h4>
                        </div>


                        <div class="pt_details">
                            <ul>
                                <li><a href="#">Brand new professionally written and fully editable CV</a></li>
                                <li><a href="#">Standard CV theme</a></li>
                                <li><a href="#">Five general letters - Cover Letter / responding to advertised position / seeking employment / Accept offer of employment / Thank You Letter</a></li>
                                <li><a href="#">Interview Tips -10 Interview Tips & Training Guides</a></li>
                                <li><a href="#">All package documents will be emailed in MS Word and PDF format</a></li>

                            </ul>
                            <a href="{{url('bookacv')}}" class="btn btn-lg btn-block"><span>  Book Your CV Now</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 p0 prpl5-sm">
                    <div class="pricing_table">
                        <h3 class="text-center">CLASSIC
CV </h3>
                        <div class="pt_header_four">
                            <div class="pt_tag_four"><span><h2 style="color:#8E6C88;">R500</h2></span></div>
                            <h4>For Applicants with Experience</h4>
                        </div>
                        <div class="pt_details">
                            <ul>
                                <li><a href="#">Brand new professionally written and fully editable CV</a></li>
                                <li><a href="#">Professionally designed CV theme</a></li>
                                <li><a href="#">Unique and effective cover letter addressing the key requirements of the position</a></li>
                                <li><a href="#">Interview Tips -10 Interview Tips & Training Guides</a></li>
                                <li><a href="#">Free future updating of minor changes*</a></li>
                            </ul>
                            <a href="{{url('bookacv')}}" class="btn btn-lg btn-block"><span>  Book Your CV Now</span></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4 p0 prpl5-sm">
                    <div class="pricing_table">
                        <h3 class="text-center">Advanced CV </h3>
                        <div class="pt_header_four">
                            <div class="pt_tag_four"><span><h2 style="color:orange;">R1200</h2></span></div>
                            <h4>For Professionals & Executives</h4>
                        </div>
                        <div class="pt_details">
                            <ul>
                                <li><a href="#">Brand new professionally written and fully editable CV</a></li>
                                <li><a href="#">Specifically for a requested targeted company.</a></li>
                                <li><a href="#">Professionally designed CV theme.</a></li>
                                <li><a href="#">Unique and effective cover letter addressing the key requirements of the position.</a></li>
                                <li><a href="#">Written according to your exact requirements</a></li>
                                <li><a href="#"> All package documents will be emailed in MS Word and PDF format
• Unlimited revisions.</a></li>
                            </ul>
                            <a href="{{url('bookacv')}}" class="btn btn-lg btn-block"><span> Book Your CV Now</span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <!-- Testimonials -->
    <section class="testimonial-section bgc-fa">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="ulockd-main-title">
                        <h3 class="mt0">We have an excellent reputation amongst our clients</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="testimonial_slider">
                        <div class="item">
                            <div class="t_icon"><span class="flaticon-quotation-mark text-thm"></span></div>
                            <div class="testimonial_post text-center">

                                <div class="client_info">
                                    <h4>Ursula Bezuidenhout</h4>
                                    <p class="text-thm">Product Manager</p>
                                </div>
                                <div class="details">
                                    <p>“Openjobs360 CV writing service is the best and l dont regret compiling my cv with them!” </p>
                                </div>
                            </div>
                        </div>


                        <div class="item">
                            <div class="t_icon"><span class="flaticon-quotation-mark text-thm"></span></div>
                            <div class="testimonial_post text-center">

                                <div class="client_info">
                                    <h4>Vuyo Sakala</h4>
                                    <p class="text-thm">C# Developer</p>
                                </div>
                                <div class="details">
                                    <p>“My CV and career profile took a new shape ever since l engaged these guys.” </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

