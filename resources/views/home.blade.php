<!DOCTYPE html>
<html lang="en">
   <!-- Mirrored from demo.ayroui.com/templates/business-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Mar 2022 00:27:58 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="x-ua-compatible" content="ie=edge" />
      <meta name="description" content="" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="shortcut icon" type="image/x-icon" href="/landing/assets/images/favicon.svg" />
      <title>Ørsted | Energy Investments</title>
      <link rel="shortcut icon" href="/landing/assets/images/favicon.html" type="image/svg" />
      <link rel="stylesheet" href="/landing/assets/css/bootstrap.min.css" />
      <link rel="stylesheet" href="/landing/assets/css/lineicons.css" />
      <link rel="stylesheet" href="/landing/assets/css/tiny-slider.css" />
      <link rel="stylesheet" href="/landing/assets/css/glightbox.min.css" />
      <link rel="stylesheet" href="/landing/style.css" />
   </head>
   <body>
      <section class="navbar-area navbar-nine">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <nav class="navbar navbar-expand-lg">
                     <a class="navbar-brand" href="{{url('/')}}">
                     <img width="150" height="70" src="/images/logo.png" alt="Logo" />
                     </a>
                     <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNine" aria-controls="navbarNine" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="toggler-icon"></span>
                     <span class="toggler-icon"></span>
                     <span class="toggler-icon"></span>
                     </button>
                     <div class="collapse navbar-collapse sub-menu-bar" id="navbarNine">
                        <ul class="navbar-nav me-auto">
                           <li class="nav-item">
                              <a class="page-scroll active" href="#hero-area">Home</a>
                           </li>
                          
                        </ul>
                     </div>
                     <div class="navbar-btn d-none d-lg-inline-block">
                        <a class="menu-bar" href="#side-menu-left"><i class="lni lni-menu"></i></a>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </section>
      <div class="sidebar-left">
         <div class="sidebar-close">
            <a class="close" href="#close"><i class="lni lni-close"></i></a>
         </div>
         <div class="sidebar-content">
            <div class="sidebar-logo">
               <a href="{{url('/')}}"><img src="/images/logo.png" alt="Logo" /></a>
            </div>
            <p class="text"></p>
            <div class="sidebar-menu">
               <h5 class="menu-title">Links</h5>
               <ul>
                  <li><a href="{{url('register')}}">Register</a></li>
                  <li><a href="{{url('login')}}">Login</a></li>
                
               </ul>
            </div>
            <div class="sidebar-social align-items-center justify-content-center">
               <h5 class="social-title">Follow Us On</h5>
               <ul>
                  <li>
                     <a href="javascript:void(0)"><i class="lni lni-facebook-filled"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="lni lni-twitter-original"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="lni lni-linkedin-original"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="lni lni-youtube"></i></a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="overlay-left"></div>
      <section id="hero-area" class="header-area header-eight">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-md-12 col-12">
                  <div class="header-content">
                     <h1>Green energy for the planet and its people.</h1>
                     <p>
                        Discover how Ørsted is making renewables a force for positive change – far beyond zero-emissions energy..
                     </p>
                     <div class="button">
                        @guest
                        <a href="{{url('login')}}" class="btn primary-btn">Login now</a>
                        @else

                        <a href="{{url('dashboard')}}" class="btn primary-btn">Dashboard</a>
                        @endguest
                        <a href="https://youtu.be/hOyDuovDLe4" class="glightbox video-button">
                        <span class="btn icon-btn rounded-full">
                        <i class="lni lni-play"></i>
                        </span>
                        <span class="text">Watch About Ørsted</span>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-12 col-12">
                  <div class="header-image">
                     <img src="/images/turbines.jpg" alt="#" />
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="about-area about-five">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-12">
                  <div class="about-image-five">
                     <svg class="shape" width="106" height="134" viewBox="0 0 106 134" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="1.66654" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="1.66654" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="16.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="16.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="16.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="16.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="16.333" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="30.9998" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6665" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="30.9998" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6665" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="30.9998" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6665" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="30.9998" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6665" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="31" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="74.6668" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="45.6665" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="89.3333" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="60.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="1.66679" r="1.66667" fill="#DADADA" />
                        <circle cx="60.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="16.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="60.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="31.0001" r="1.66667" fill="#DADADA" />
                        <circle cx="60.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="45.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="60.3335" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="88.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="117.667" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="74.6668" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="103" r="1.66667" fill="#DADADA" />
                        <circle cx="60.333" cy="132" r="1.66667" fill="#DADADA" />
                        <circle cx="104" cy="132" r="1.66667" fill="#DADADA" />
                     </svg>
                     <img src="/images/roi.jpg" alt="about" />
                  </div>
               </div>
               <div class="col-lg-6 col-12">
                  <div class="about-five-content">
                     <h6 class="small-title text-lg">OUR INVESTMENT PLAN</h6>
                     <h2 class="main-title fw-bold">We have a team of experts responsible for managing investments.</h2>
                     <div class="about-five-tab">
                        
                        <div class="tab-content" id="nav-tabContent">
                           <div class="tab-pane fade show active" id="nav-who" role="tabpanel" aria-labelledby="nav-who-tab">
                              <p>We have a well curated investment portfolio, to allow anyone with or without any investment knowledge to participate and benefit.
                              </p>
                              <p>As a user, you have full controll over your account and can perfom any task.
                              </p>
                           </div>
                           <div class="tab-pane fade" id="nav-vision" role="tabpanel" aria-labelledby="nav-vision-tab">
                              <p>It is a long established fact that a reader will be distracted by the readable content of a page
                                 when
                                 looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                                 distribution of letters, look like readable English.
                              </p>
                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have in some
                                 form,
                                 by injected humour.
                              </p>
                           </div>
                           <div class="tab-pane fade" id="nav-history" role="tabpanel" aria-labelledby="nav-history-tab">
                              <p>It is a long established fact that a reader will be distracted by the readable content of a page
                                 when
                                 looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                                 distribution of letters, look like readable English.
                              </p>
                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have in some
                                 form,
                                 by injected humour.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  
     
      <section id="call-action" class="call-action">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col-xxl-6 col-xl-7 col-lg-8 col-md-9">
                  <div class="inner-content">
                     <h2>Join our investment Groups<br />and get live information</h2>
                     <p>
                        
                     </p>
                     <div class="light-rounded-buttons">
                        <a href="https://chat.whatsapp.com/JSvp040biJK62I5I5MK2fh" class="btn primary-btn-outline">Whatsapp Group</a>
                     </div>
                      <div class="light-rounded-buttons">
                        <a href="https://t.me/OrstedGreenEnergyInvestment" class="btn btn-danger">Telegram Group</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
     
      <footer class="footer-area footer-eleven">
         <div class="footer-top">
            <div class="container">
               <div class="inner-content">
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-12">
                        <div class="footer-widget f-about">
                           <div class="logo">
                              <a href="index.html">
                              <img src="/images/logo.png" alt="#" class="img-fluid" />
                              </a>
                           </div>
                           <p>
                              The future belongs to you.
                           </p>
                           <p class="copyright-text">
                              <span>© 2022 Orsted.</span>Designed and Developed by
                              <a href="javascript:void(0)" rel="nofollow"> Orsted </a>
                           </p>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-6 col-12">
                        <div class="footer-widget f-link">
                           <h5>Account</h5>
                           <ul>
                              <li><a href="{{url('register')}}">Register</a></li>
                              
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-6 col-12">
                        <div class="footer-widget f-link">
                           <h5>Navigation</h5>
                           <ul>
                              <li><a href="{{url('dashboard')}}">Dashboard</a></li>
                              
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-12">
                        <div class="footer-widget newsletter">
                           <h5>Subscribe</h5>
                           <p>Subscribe  for the latest updates</p>
                           <form action="#" method="get" target="_blank" class="newsletter-form">
                              <input name="EMAIL" placeholder="Email address" required="required" type="email" />
                              <div class="button">
                                 <button class="sub-btn">
                                 <i class="lni lni-envelope"></i>
                                 </button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
     
      <a href="#" class="scroll-top btn-hover">
      <i class="lni lni-chevron-up"></i>
      </a>
      <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="/landing/assets/js/bootstrap.bundle.min.js" type="498c35da73a264fe3116a89e-text/javascript"></script>
      <script src="/landing/assets/js/glightbox.min.js" type="498c35da73a264fe3116a89e-text/javascript"></script>
      <script src="/landing/assets/js/main.js" type="498c35da73a264fe3116a89e-text/javascript"></script>
      <script src="/landing/assets/js/tiny-slider.js" type="498c35da73a264fe3116a89e-text/javascript"></script>
      <script type="text/javascript">
         //===== close navbar-collapse when a  clicked
         let navbarTogglerNine = document.querySelector(
           ".navbar-nine .navbar-toggler"
         );
         navbarTogglerNine.addEventListener("click", function () {
           navbarTogglerNine.classList.toggle("active");
         });
         
         // ==== left sidebar toggle
         let sidebarLeft = document.querySelector(".sidebar-left");
         let overlayLeft = document.querySelector(".overlay-left");
         let sidebarClose = document.querySelector(".sidebar-close .close");
         
         overlayLeft.addEventListener("click", function () {
           sidebarLeft.classList.toggle("open");
           overlayLeft.classList.toggle("open");
         });
         sidebarClose.addEventListener("click", function () {
           sidebarLeft.classList.remove("open");
           overlayLeft.classList.remove("open");
         });
         
         // ===== navbar nine sideMenu
         let sideMenuLeftNine = document.querySelector(".navbar-nine .menu-bar");
         
         sideMenuLeftNine.addEventListener("click", function () {
           sidebarLeft.classList.add("open");
           overlayLeft.classList.add("open");
         });
         
         //========= glightbox
         GLightbox({
           'href': 'https://www.youtube.com/watch?v=r44RKWyfcFw&fbclid=IwAR21beSJORalzmzokxDRcGfkZA1AtRTE__l5N4r09HcGS5Y6vOluyouM9EM',
           'type': 'video',
           'source': 'youtube', //vimeo, youtube or local
           'width': 900,
           'autoplayVideos': true,
         });
         
         //============== isotope masonry portfolio-three
         const filters = document.querySelectorAll(".portfolio-menu button");
         
         filters.forEach((filter) => {
           filter.addEventListener("click", function () {
             // ==== Filter btn toggle
             let filterBtn = filters[0];
             while (filterBtn) {
               if (filterBtn.tagName === "BUTTON") {
                 filterBtn.classList.remove("active");
               }
               filterBtn = filterBtn.nextSibling;
             }
             this.classList.add("active");
         
             // === filter
             let selectedFilter = filter.getAttribute("data-filter");
             let itemsToHide = document.querySelectorAll(
               `.grid .col-lg-4:not([data-filter='${selectedFilter}'])`
             );
             let itemsToShow = document.querySelectorAll(
               `.grid [data-filter='${selectedFilter}']`
             );
         
             if (selectedFilter == "all") {
               itemsToHide = [];
               itemsToShow = document.querySelectorAll(".grid [data-filter]");
             }
         
             itemsToHide.forEach((el) => {
               el.classList.add("hide");
               el.classList.remove("show");
             });
         
             itemsToShow.forEach((el) => {
               el.classList.remove("hide");
               el.classList.add("show");
             });
           });
         });
         
         //========= glightbox
         const myGallery3 = GLightbox({
           selector: ".glightbox3",
           type: "image",
           width: 900,
         });
         
         //========= testimonial
         tns({
           container: ".testimonial-slider",
           items: 3,
           autoplay: true,
           autoplayButtonOutput: false,
           mouseDrag: true,
           gutter: 0,
           nav: true,
           controls: false,
           controlsText: [
             '<i class="lni lni-arrow-left"></i>',
             '<i class="lni lni-arrow-right"></i>',
           ],
           responsive: {
             0: {
               items: 1,
             },
             1170: {
               items: 2,
             },
           },
         });
         
      </script>
      <script src="/cdn-cgi/scripts/7d0fa10a/cloudflare-static/rocket-loader.min.js" data-cf-settings="498c35da73a264fe3116a89e-|49" defer=""></script>
   </body>
   <!-- Mirrored from demo.ayroui.com/templates/business-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Mar 2022 00:28:47 GMT -->
</html>