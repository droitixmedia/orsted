@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')          
                           <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="nk-block-head">
                                <div class="nk-block-between-md g-4">
                                    <div class="nk-block-head-content">
                                        <h2 class="nk-block-title fw-normal">Payment Methods</h2>
                                        <div class="nk-block-des">
                                            <p>Once you pay return to your order and request approval</p>
                                        </div>
                                    </div>
                                    <div class="nk-block-head-content">
                                        <ul class="nk-block-tools gx-3">
                                            <li class="order-md-last"><a href="{{ route('listings.published.index', [$area]) }}" class="btn btn-primary"><span>Return to Order</span> <em class="icon ni ni-arrow-long-left"></em></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head -->
                            <ul class="nk-nav nav nav-tabs">
                               
                               
                            </ul><!-- .nav-tabs -->
                            <div class="nk-block nk-block-sm">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        
                                    </div>
                                    <div class="search-wrap search-wrap-extend" data-search="search">
                                        <div class="search-content">
                                            <a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
                                            <input type="text" class="form-control form-control-sm border-transparent form-focus-none" placeholder="Quick search by user">
                                            <button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
                                        </div>
                                    </div><!-- .search-wrap -->
                                </div><!-- .nk-block-head -->
                                <h6 class="lead-text text-soft">Payment methods available in your country</h6>
                                <div class="tranx-list tranx-list-stretch card card-bordered">
                                    <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                       <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">{{ Auth::user()->area->payment1 }}</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                                <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div><!-- .nk-tranx-item -->
                                    <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                        <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">{{ Auth::user()->area->payment2 }}</div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                            <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                        <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">{{ Auth::user()->area->icon }}</div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                              <img src="./images/coins/btc.svg" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">Bitcoin: bc1qrxgwxyaqcdznntwnr5fn4htz35vnfkgt3p08x9</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                              <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                              <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">Paypal: catalystincome@gmail.com</div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                              <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">Skrill: catalystincome@gmail.com</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tranx-item">
                                        <div class="tranx-col">
                                            <div class="tranx-info">
                                                <div class="tranx-badge">
                                                    <span class="tranx-icon">
                                                              <img src="./images/favicon.png" alt="">
                                                    </span>
                                                </div>
                                                <div class="tranx-data">
                                                    <div class="tranx-label">Tron: {{ Auth::user()->area->paypal }}</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tranx-col">
                                            <div class="tranx-amount">
                                               <div class="number"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-btc"></span></div>
                                                <div class="number-sm"><em class="icon ni ni-arrow-long-left"></em><span class="currency currency-usd"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .card -->
                                
                                </div><!-- .card -->
                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->


@endsection
